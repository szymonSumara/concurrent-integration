**IGA-FEM** - jest to rozwinięcie metody elementów skończonych, z tą różnicą, że w wersji izogeometrycznej geometria jest za pomocą **funkcji sklejanych (B-Spline)**.

Dzięki zastosowaniu tej metody, izogeometryczna analiza umożliwia bardziej precyzyjne odwzorowanie geometrii obiektów, ponieważ krzywe i powierzchnie B-sklejane są elastycznymi narzędziami do reprezentacji skomplikowanych kształtów. Dodatkowo, izogeometryczna metoda może korzystać z wysokich stopni interpolacji, co prowadzi do uzyskania bardziej dokładnych wyników analizy.

**Teoria śladów** - jest jedną z wielu metod używanych w formalnej weryfikacji obliczeń współbieżnych. Jest to metoda, która dostarcza postaci normalnej Foata (FNF) oraz grafów zależności Diekerta. W naszym problemie teoria śladów pomaga opisać przetwarzanie w pojedynczym elemencie i uprościć równoległą implementację na maszynach równoległych, takich jak karty graficzne. Teoria śladów stanowi teoretyczną podstawę do weryfikacji poprawności algorytmów równoległych.

**Kwadratury Gaussa** - to metoda numeryczna używana do obliczania wartości całek. Metoda ta opiera się na wyborze odpowiednich węzłów i wag, które są używane do obliczenia ważonej sumy wartości funkcji na tych węzłach. Jest oparta na specjalnych wzorach, które umożliwiają dokładne przybliżenie wyników nawet dla skomplikowanych funkcji. Należy wspomnieć, że liczba punktów jest linowo zależna od stopnia wielomianu, którego całkę obliczamy.

**Weighted quadrature (kwadratury ważone)** - dzięki zastosowaniu kwadratur warzonych jesteśmy w stanie obliczać wartości całki w czasie stałym korzystając ze stałej liczby punktów, której liczba wynosi 3 punkty w każdym wymiarze.

**Sum factorization** - metody całkowania polegające na zmianie kolejności wyliczania sum iloczynów funkcji test, trial i punktów kwadratury w taki sposób, że obniżamy złożoność teoretyczną całego algorytmu. Jednak zastosowanie tej optymalizacji wprowadza pewnie niedogodności, jak konieczność spamiętywania wcześniejszych sum cząstkowych oraz wykonywania większej ilości synchronizacja podczas przetwarzania równoległego.

**Assembly row by row** - metody całkowania różnicą się od pozostałych kolejnością mnożeń funkcji test i trial. Dla każdej funkcji test przechodzimy tylko po funkcjach trial , których support pokrywa się z funkcją trial, w ten sposób pomijamy zdecydowaną większość miejsc, gdzie B-spliny przyjmują wartość 0.

Połączenie tej metody z sum factorization i ważonymi kwadraturami Gausa pozwala osiągnąć znaczący spadek złożoności obliczeniowej wewnątrz elementu.

# Wyprowadzenie złożoności algorytmów całkowania z wykorzystaniem powyższych metod

W kolejnych krokach pokażemy jak metody **Sum factorization**, **Assembly row by row** oraz **kwadratury ważone** mogą zostać wykorzystane w celu zmniejszenia teoretycznej złożoności algorytmów całkowania. W wyprowadzeniach skupimy się na problemach w przestrzeni trójwymiarowej.

## Algorytm z zastosowaniem **kwadratur Gaussa**

W pierwszych iteracjach przechodzimy przechodzimy po funkcjach test i trial w wszystkich kierunkach. W kolejnych krokach iterujemy po wszystkich punktach kwadratury w każdym kierunku co daje nam **O(p<sup>3</sup>)** punktów gdzie **p** jest stopniem wielomianu. Finalnie otrzymujemy złożoność **O(p<sup>9</sup>)**.

```python

for test_function in B(i,x):
    for trial_function in B(j,x):
        for test_function in B(i,y):
            for trial_function in B(j,y):
                for test_function in B(i,z):
                    for trial_function in B(j,z):
                        for quadrature_point(ξ_x,w_x)w in K:
                            for quadrature_point(ξ_y,w_y)w in K:
                                for quadrature_point(ξ_z,w_z)w in K:
                                    A(i,j) = A(i,j) + B(i,x)(ξ_x)*B(i,j)(ξ_y)*
                                    B(j,y)(ξ_y)*B(i,z)(ξ_z)B(j,z)(ξ_z)*wx*wy*wz*c(ξ)



```

Dokładniej analizując złożoność obliczeniową otrzymujemy:

<p style="margin-left:5em;">

m<sup>3</sup>*n<sup>3</sup>*q<sup>3</sup> = <b>O(p<sup>9</sup>)</b>

</p>

gdzie:

<p style="margin-left:5em;margin-bottom:5em;"> 
m = p+1 - liczba funkcji test <br> 
n = p+1 - liczba funkcji trial<br> 
q = p+1 - liczba punktów kwadratury w jendym kierunku 
</p>

### Algorytm Z zastosowaniem **kwadratur ważonych**

Wykorzystując kwadratury ważone jesteśmy w stanie w znacznym stopniu zmniejszyć złożoność standardowej wersji całkowania. Teraz nie potrzebujemy już używać **O(p)** punktów do policzenia kwadratury w jednym kierunku a tylko stałej ich liczby. W ten sposób otrzymujemy złożoność równą **O(p<sup>6</sup>)**

```javascript

for test_function in B(i,x):
    for trial_function in B(j,x):
        for test_function in B(i,y):
            for trial_function in B(j,y):
                for test_function in B(i,z):
                    for trial_function in B(j,z):
                        A(i,j) = A(i,j) + B(i,x)(ξ_x)*B(i,j)(ξ_y)*
                        B(j,y)(ξ_y)*B(i,z)(ξ_z)B(j,z)(ξ_z)*wx*wy*wz*c(ξ)
```

Dokładniej analizując złożoność obliczeniową otrzymujemy:

<p style="margin-left:5em;">

m<sup>3</sup>n<sup>3</sup> = <b>O(p<sup>6</sup>)</b>

</p>

gdzie:

<p style="margin-left:5em;margin-bottom:5em;"> 
m = p+1 - liczba funkcji test <br> 
n = p+1 - liczba funkcji trial<br> 
q = O(1) - liczba punktów kwadratury w jendym kierunku 
</p>

## Algorytm z zastosowaniem metody **sum factorization** i **kwadratur ważonych**

Jak było wspomiane wcześniej **sum factorization** jest to metoda pozalająca na obniżeniu teoretycznej złożoniości całkowania używana w metodzie elemenetów skończonych. Polega ona na wyliczaniu sum cząstkowych a następnie kożystanie z nich. Dzięki tym przekształcenią jesteśmy w stanie zredukować złożonośc obliczeniową z **O(p<sup>9</sup>)** do **O(p<sup>7</sup>)** a, po zastosowoaniu ważonych kwadratur redukujemy tą złożoność dodatkowo do **O(p<sup>6</sup>)**

Poniższy fragment kodu przedstawia algorytm całkowania z wykożystaniem metody **Sum factorization** dla kwadratur Gaussa

```javascript

for test_function in B(i,z) - i3:
    for trial_function in B(j,z) -j3:
        for quadrature_point(ξ_x,w_x)w in K - k1:
            for quadrature_point(ξ_y,w_y)w in K - k2:
                for quadrature_point(ξ_z,w_z)w in K - k3:
                    C(i3,j3,k1,k2) = C(i3,j3,k1,k2) + B(i,z)(ξ_z)*B(j,z)(ξ_z)*wz*c(ξ)

for test_function in B(i,y) -i2:
    for trial_function in B(j,y) -j2:
        for test_function in B(i,z) -i3:
            for trial_function in B(j,z) -j3:
                for quadrature_point(ξ_x,w_x)w in K -k1:
                    for quadrature_point(ξ_y,w_y)w in K -k2:
                        D(i2,i3,j2,j3,k1) = (
                        D(i2,i3,j2,j3,k1)
                        + B(i,y)(ξ_y)*B(j,y)(ξ_y)*C(i3,j3,k1,k2)*wy
                        )


for test_function in B(i,x) - i1:
    for trial_function in B(j,x) -j1:
        for test_function in B(i,y) - i2:
            for trial_function in B(j,y) -j2:
                for test_function in B(i,z) -i3:
                    for trial_function in B(j,z) -j3:
                        for quadrature_point(ξ_x,w_x)w in K -k1:
                            A(i,j) = (
                            A(i,j)
                                + B(i,x)(ξ_x)*B(i,j)(ξ_y)*D(i2,i3,j2,j3,k1*wx)
                            )

```

Dokładniej analizując złożoność obliczeniową otrzymujemy:

<p style="margin-left:5em;">

m<sup>3</sup>n<sup>3</sup>q + m<sup>2</sup>n<sup>2</sup>q<sup>2</sup> + mnq<sup>3</sup>= <b>O(p<sup>7</sup>)</b>

</p>

gdzie:

<p style="margin-left:5em;margin-bottom:5em;"> 
m = p+1 - liczba funkcji test <br> 
n = p+1 - liczba funkcji trial<br> 
q = p+1 - liczba punktów kwadratury w jendym kierunku 
</p>

Poniższy fragment kodu przedstawia algorytm całkowania z wykożystaniem metody **Sum factorization** dla kwadratur ważonych

```javascript

for test_function in B(i,z) - i3:
    for trial_function in B(j,z) -j3:
        for quadrature_point(ξ_x,w_x)w in (3 points) - k1:
            for quadrature_point(ξ_y,w_y)w in (3 points) - k2:
                for quadrature_point(ξ_z,w_z)w in (3 points) - k3:
                    C(i3,j3,k1,k2) = (
                    C(i3,j3,k1,k2)
                    + B(i,z)(ξ_z)*B(j,z)(ξ_z)*wz*c(ξ)
                    )

for test_function in B(i,y) -i2:
    for trial_function in B(j,y) -j2:
        for test_function in B(i,z) -i3:
            for trial_function in B(j,z) -j3:
                for quadrature_point(ξ_x,w_x)w in (3 points) - k1:
                    for quadrature_point(ξ_y,w_y)w in (3 points) - k2:
                        D(i2,i3,j2,j3,k1) = (
                        D(i2,i3,j2,j3,k1)
                        + B(i,y)(ξ_y)*B(j,y)(ξ_y)*C(i3,j3,k1,k2)*wy
                        )


for test_function in B(i,x) - i1:
    for trial_function in B(j,x) -j1:
        for test_function in B(i,y) - i2:
            for trial_function in B(j,y) -j2:
                for test_function in B(i,z) -i3:
                    for trial_function in B(j,z) -j3:
                        for quadrature_point(ξ_x,w_x)w in (3 points) - k1:
                            A(i,j) = (
                            A(i,j)
                            + B(i,x)(ξ_x)*B(i,j)(ξ_y)*D(i2,i3,j2,j3,k1)*wx
                            )

```

Dokładniej analizując złożoność obliczeniową otrzymujemy:

<p style="margin-left:5em;"> 
m<sup>3</sup>n<sup>3</sup>q + m<sup>2</sup>n<sup>2</sup>q<sup>2</sup> + mnq<sup>3</sup>= <b>O(p<sup>6</sup>)</b>

</p>

gdzie:

<p style="margin-left:5em;margin-bottom:5em;"> 
m = p+1 - liczba funkcji test <br> 
n = p+1 - liczba funkcji trial<br> 
q = O(1) - liczba punktów kwadratury w jendym kierunku 
</p>

# Zastosowanie metody **Assembly row by row** dla powyrzszych algorytmów

Metoda **Assembly row by row** wprowadza zmianę polegającą na iteracji po **supportach** zamiast po elementach. Dodatkowo nie liczymy wartości gdzie dwa **B-spline** nie posiadają części wspólnej.

```
for support K of some test function do
    for test_function in B(i,x) with supp B(i,x) = K:
        for trial function B(j,x) overlapping B(i,x):
            for quadrature point (ξ,w) in supp B(j,x) ∩ supp B(i,x) do
                A(i, j) = A(i, j) + B(i,ξ)B(j,ξ)(ξ)w
```

Powoduje to zmianę w ilości:

<p style="margin-left:5em;margin-bottom:5em;"> 
m = 1 - liczba funkcji test <br> 
n = 2p+1 - liczba funkcji trial<br> 
q = O(p+1)<sup>2</sup> - liczba punktów kwadratury dla kwadratur Gaussa<br> 
q = O(p+1) - liczba punktów kwadratury dla kwadratur ważonych 
</p>

Stosując powyższą metodę do wcześniej przedstawionych algorytmów otrzymujemy kolejno:

- dla algorytmu z kwadraturami Gaussa
<p style="margin-left:5em;"> 
m<sup>3</sup>n<sup>3</sup>q<sup>3</sup> = (2p+1)<sup>3</sup>*(p+1)<sup>6</sup> = <b>O(p<sup>9</sup>)</b> 
</p>

- dla algorytmu z kwadraturami ważonymi
<p style="margin-left:5em;"> 
m<sup>3</sup>n<sup>3</sup>q<sup>3</sup> = (2p+1)<sup>3</sup>*(p+1)<sup>3</sup> = <b>O(p<sup>6</sup>)</b> 
</p>

- dla algorytmu z kwadraturami ważonymi oraz metodą sum factorization

<p style="margin-left:5em;"> 
m<sup>3</sup>n<sup>3</sup>q + m<sup>2</sup>n<sup>2</sup>q<sup>2</sup> + mnq<sup>3</sup> <br>=  
(2p+1)<sup>3</sup>(p + 1) + (2p+1)<sup>2</sup>(p + 1)<sup>2</sup> + (2p+1)(p + 1)<sup>3</sup>=<b>O(p<sup>4</sup>)</b> 
</p>

## Podsumowanie złożoności poszczególnych metod całkowania

#### Kwadratury Gaussa

|            |        2D        |        3D        |
| :--------: | :--------------: | :--------------: |
| by element | O(p<sup>6</sup>) | O(p<sup>9</sup>) |
| row by row | O(p<sup>6</sup>) | O(p<sup>9</sup>) |

#### Kwadratury Ważone

|            |        2D        |        3D        |
| :--------: | :--------------: | :--------------: |
| by element | O(p<sup>4</sup>) | O(p<sup>6</sup>) |
| row by row | O(p<sup>4</sup>) | O(p<sup>6</sup>) |

#### Sum factorization kwadratury ważone

|            |        2D        |        3D        |
| :--------: | :--------------: | :--------------: |
| by element | O(p<sup>4</sup>) | O(p<sup>6</sup>) |
| row by row | O(p<sup>3</sup>) | O(p<sup>4</sup>) |
