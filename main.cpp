#include <iostream>
#include <math.h>
#include <sstream>
#include <iomanip>
#include <cmath>
#include <fstream>

#include "quadrature_points.h"

struct Vector3D {
    int x;
    int y;
    int z;
};

struct Spline {
    int x;
    int y;
    int z;
};

struct Element {
    int x;
    int y;
    int z;
};

struct Interval {
    Vector3D start;
    Vector3D end;
};

double START = 0.0;
double END = 1.0;

const int INTERVAL_NUMBER_IN_DIMENSION = 3;
const int SPLINE_DEGREE = 2;
const int SPLINE_NUMBER_IN_DIMENSION = INTERVAL_NUMBER_IN_DIMENSION + SPLINE_DEGREE;
const int SPLINE_NUMBER = std::pow(INTERVAL_NUMBER_IN_DIMENSION + SPLINE_DEGREE, 3);

int QUADRATURE_POINTS_NUMBER = 2;
const double *QUADRATURE_POINTS = Xs[QUADRATURE_POINTS_NUMBER];
const double *QUADRATURE_WEIGHTS = Ws[QUADRATURE_POINTS_NUMBER];

double **MAT;
double *knot;
double ***b;
double *J;

void initialize_matrix() {
    MAT = new double *[SPLINE_NUMBER];
    for (int i = 0; i < SPLINE_NUMBER; i++) {
        MAT[i] = new double[SPLINE_NUMBER];
    }
    std::cout << "initialized_matrix" << std::endl;
}

void clear_matrix() {
    for (int i = 0; i < SPLINE_NUMBER; i++) {
        delete MAT[i];
    }
    delete MAT;
}

void initialize_knot() {
    knot = new double[INTERVAL_NUMBER_IN_DIMENSION + 1 + SPLINE_DEGREE * 2];
    int index = 0;

    for (int i = 0; i < SPLINE_DEGREE; i++) {
        knot[index] = START;
        index++;
    }

    for (int i = 0; i < INTERVAL_NUMBER_IN_DIMENSION + 1; i++) {
        knot[index] = START + i * (END - START) / INTERVAL_NUMBER_IN_DIMENSION;
        index++;
    }

    for (int i = 0; i < SPLINE_DEGREE; i++) {
        knot[index] = END;
        index++;
    }
    std::cout << "initialized_knot" << std::endl;
}

void eval_basis(int i, double x, double *out) {
    int p = SPLINE_DEGREE;
    double *left = new double[p + 1];
    double *right = new double[p + 1];

    out[0] = 1;
    for (int j = 1; j <= p; ++j) {
        left[j] = x - knot[i + 1 - j];
        right[j] = knot[i + j] - x;
        double saved = 0;

        for (int r = 0; r < j; ++r) {
            double tmp = out[r] / (right[r + 1] + left[j - r]);
            out[r] = saved + right[r + 1] * tmp;
            saved = left[j - r] * tmp;
        }
        out[j] = saved;
    }
}

void compute_bspline_values() {
    ;
    J = new double[INTERVAL_NUMBER_IN_DIMENSION];
    b = new double **[INTERVAL_NUMBER_IN_DIMENSION];
    for (int e = 0; e < INTERVAL_NUMBER_IN_DIMENSION; e++) {
        b[e] = new double *[QUADRATURE_POINTS_NUMBER];
        double x1 = knot[e + SPLINE_DEGREE];
        double x2 = knot[e + SPLINE_DEGREE + 1];
        J[e] = 0.5 * (x2 - x1);
        for (int x = 0; x < QUADRATURE_POINTS_NUMBER; x++) {
            b[e][x] = new double[SPLINE_DEGREE + 1];
            double point = QUADRATURE_POINTS[x];
            double new_point = (point + 1.0) * J[e] + x1;

            eval_basis(e + SPLINE_DEGREE, new_point, b[e][x]);
        }
    }
    std::cout << "initialized_b" << std::endl;
}

void clear_b() {
    for (int e = 0; e < INTERVAL_NUMBER_IN_DIMENSION; e++) {
        for (int k = 0; k < SPLINE_DEGREE; k++) {
            delete[] b[e][k];
        }
        delete[] b[e];
    }
    delete[] b;
    delete[] J;
}

void clear_knot() {
    delete knot;
}

double spline(int i, double point, int p = SPLINE_DEGREE) {
    double intervalSize = (END - START) / INTERVAL_NUMBER_IN_DIMENSION;
    double ti = knot[i];
    double ti_incr = knot[i + 1];
    double tip = knot[i + p];
    double tip_incr = knot[i + p + 1];

    if (p == 0) {
        if (ti <= point && point < ti_incr) {
            return 1;
        }
        return 0;
    }

    double alpha;
    double beta;

    if (tip != ti) {
        alpha = (point - ti) / (tip - ti);
    } else {
        alpha = 0;
    }

    if (tip_incr != ti_incr) {
        beta = (tip_incr - point) / (tip_incr - ti_incr);
    } else {
        beta = 0;
    }

    return alpha * spline(i, point, p - 1) + beta * spline(i + 1, point, p - 1);
}


double splineDX(int i, double point, int p = SPLINE_DEGREE) {
    double ti = knot[i];
    double ti_incr = knot[i + 1];
    double tip = knot[i + p];
    double tip_incr = knot[i + p + 1];

    double alpha;
    double beta;

    if (tip != ti) {
        alpha = p / (tip - ti);
    } else {
        alpha = 0;
    }

    if (tip_incr != ti_incr) {
        beta = p / (tip_incr - ti_incr);
    } else {
        beta = 0;
    }

    return alpha * spline(i, point, p - 1) - beta * spline(i + 1, point, p - 1);
}

int getElementIndexFromSplineIndex(int spline) {
    return std::min(std::max(0, spline - SPLINE_DEGREE), INTERVAL_NUMBER_IN_DIMENSION);
}

double B_part(int element, int test, int trial, int point) {
    return b[element][point][test - element] *
           b[element][point][trial - element];
}

double B(Element element, Spline test, Spline trial, Vector3D point) {
    return B_part(element.x, test.x, trial.x, point.x) * J[element.x] *
           B_part(element.y, test.y, trial.y, point.y) * J[element.y] *
           B_part(element.z, test.z, trial.z, point.z) * J[element.z];
}

double calculateBInEachDimension(int intervalStart, int intervalEnd, int test, int trial, double point) {
    double sum = 0.0;
    for (int i = getElementIndexFromSplineIndex(intervalStart); i < getElementIndexFromSplineIndex(intervalEnd); i++) {
        sum += B_part(i, test, trial, point) * J[i];
    }
    return sum;
}

double B(Interval interval, Spline test, Spline trial, Vector3D point) {
    return calculateBInEachDimension(interval.start.x, interval.end.x, test.x, trial.x, point.x) *
           calculateBInEachDimension(interval.start.y, interval.end.y, test.y, trial.y, point.y) *
           calculateBInEachDimension(interval.start.z, interval.end.z, test.z, trial.z, point.z);
}

double L(int i, double x) {
    double end = knot[i + SPLINE_DEGREE + 1];
    double start = knot[i];
    x = ((end - start) / 2) * x + (start + end) / 2;
    return ((end - start) / 2) * (x * spline(i, x));
}

Element get_element(int index) {
    Element element;
    element.x = index % INTERVAL_NUMBER_IN_DIMENSION;
    index /= INTERVAL_NUMBER_IN_DIMENSION;
    element.y = index % INTERVAL_NUMBER_IN_DIMENSION;
    index /= INTERVAL_NUMBER_IN_DIMENSION;
    element.z = index;
    return element;
}

int getSplineIndex(int x, int y, int z) {
    int idx = z;
    idx *= (INTERVAL_NUMBER_IN_DIMENSION + SPLINE_DEGREE);
    idx += y;
    idx *= (INTERVAL_NUMBER_IN_DIMENSION + SPLINE_DEGREE);
    idx += x;
    return idx;
}

Interval getCommonInterval(int x_test, int x_trial, int y_test, int y_trial, int z_test, int z_trial) {
    Interval interval;
    interval.start.x = std::max(x_test, x_trial);
    interval.start.y = std::max(y_test, y_trial);
    interval.start.z = std::max(z_test, z_trial);

    interval.end.x = std::min(x_test + SPLINE_DEGREE + 1, x_trial + SPLINE_DEGREE + 1);
    interval.end.y = std::min(y_test + SPLINE_DEGREE + 1, y_trial + SPLINE_DEGREE + 1);
    interval.end.z = std::min(z_test + SPLINE_DEGREE + 1, z_trial + SPLINE_DEGREE + 1);

    return interval;
}

void classicIntegration() {
    // via elements
    for (int element_index = 0; element_index < std::pow(INTERVAL_NUMBER_IN_DIMENSION, 3); element_index++) {
        Element element = get_element(element_index);

        for (int i_x = element.x; i_x <= element.x + SPLINE_DEGREE; i_x++)
            for (int j_x = element.x; j_x <= element.x + SPLINE_DEGREE; j_x++)
                for (int i_y = element.y; i_y <= element.y + SPLINE_DEGREE; i_y++)
                    for (int j_y = element.y; j_y <= element.y + SPLINE_DEGREE; j_y++)
                        for (int i_z = element.z; i_z <= element.z + SPLINE_DEGREE; i_z++)
                            for (int j_z = element.z; j_z <= element.z + SPLINE_DEGREE; j_z++) {
                                int test_index = getSplineIndex(i_x, i_y, i_z);
                                int trial_index = getSplineIndex(j_x, j_y, j_z);

                                Spline test = {i_x, i_y, i_z};
                                Spline trial = {j_x, j_y, j_z};

                                for (int point_x = 0; point_x < QUADRATURE_POINTS_NUMBER; point_x++)
                                    for (int point_y = 0; point_y < QUADRATURE_POINTS_NUMBER; point_y++)
                                        for (int point_z = 0; point_z < QUADRATURE_POINTS_NUMBER; point_z++) {
                                            Vector3D point = {point_x, point_y, point_z};
                                            MAT[test_index][trial_index] +=
                                                    B(element, test, trial, point) * QUADRATURE_WEIGHTS[point_x] *
                                                    QUADRATURE_WEIGHTS[point_y] * QUADRATURE_WEIGHTS[point_z];
                                        }
                            }
    }
}

void sumFactorizationIntegration() {
    int spline_num = INTERVAL_NUMBER_IN_DIMENSION + SPLINE_DEGREE;
    int point_num = 2;
    double ****C = new double ***[spline_num];

    for (int i = 0; i < spline_num; i++) {
        C[i] = new double **[spline_num];
        for (int j = 0; j < spline_num; j++) {
            C[i][j] = new double *[point_num];
            for (int k = 0; k < point_num; k++) {
                C[i][j][k] = new double[point_num];
                for (int m = 0; m < point_num; m++) {
                    C[i][j][k][m] = 0;
                }
            }
        }
    }

    double *****D = new double ****[spline_num];

    for (int i = 0; i < spline_num; i++) {
        D[i] = new double ***[spline_num];
        for (int j = 0; j < spline_num; j++) {
            D[i][j] = new double **[spline_num];
            for (int k = 0; k < spline_num; k++) {
                D[i][j][k] = new double *[spline_num];
                for (int l = 0; l < spline_num; l++) {
                    D[i][j][k][l] = new double[point_num];
                    for (int m = 0; m < point_num; m++) {
                        D[i][j][k][l][m] = 0;
                    }
                }
            }
        }
    }

    for (int element_z = 0; element_z < INTERVAL_NUMBER_IN_DIMENSION; element_z++)
        for (int i_z = element_z; i_z <= element_z + SPLINE_DEGREE; i_z++)
            for (int j_z = element_z; j_z <= element_z + SPLINE_DEGREE; j_z++)
                for (int point_x = 0; point_x < QUADRATURE_POINTS_NUMBER; point_x++)
                    for (int point_y = 0; point_y < QUADRATURE_POINTS_NUMBER; point_y++)
                        for (int point_z = 0; point_z < QUADRATURE_POINTS_NUMBER; point_z++)
                            C[i_z][j_z][point_x][point_y] += B_part(element_z, i_z, j_z, point_z) *
                                                             QUADRATURE_WEIGHTS[point_z] *
                                                             J[element_z];

    for (int element_y = 0; element_y < INTERVAL_NUMBER_IN_DIMENSION; element_y++)
        for (int i_y = element_y; i_y <= element_y + SPLINE_DEGREE; i_y++)
            for (int j_y = element_y; j_y <= element_y + SPLINE_DEGREE; j_y++)
                for (int i_z = 0; i_z < INTERVAL_NUMBER_IN_DIMENSION + SPLINE_DEGREE; i_z++)
                    for (int j_z = 0; j_z < INTERVAL_NUMBER_IN_DIMENSION + SPLINE_DEGREE; j_z++)
                        for (int point_x = 0; point_x < QUADRATURE_POINTS_NUMBER; point_x++)
                            for (int point_y = 0; point_y < QUADRATURE_POINTS_NUMBER; point_y++) {
                                D[i_y][j_y][i_z][j_z][point_x] += C[i_z][j_z][point_x][point_y] *
                                                                  B_part(element_y, i_y, j_y,
                                                                         point_y) *
                                                                  QUADRATURE_WEIGHTS[point_y] *
                                                                  J[element_y];
                            }

    for (int element_x = 0; element_x < INTERVAL_NUMBER_IN_DIMENSION; element_x++)
        for (int i_x = element_x; i_x <= element_x + SPLINE_DEGREE; i_x++)
            for (int j_x = element_x; j_x <= element_x + SPLINE_DEGREE; j_x++)
                for (int i_y = 0; i_y < INTERVAL_NUMBER_IN_DIMENSION + SPLINE_DEGREE; i_y++)
                    for (int j_y = 0; j_y < INTERVAL_NUMBER_IN_DIMENSION + SPLINE_DEGREE; j_y++)
                        for (int i_z = 0; i_z < INTERVAL_NUMBER_IN_DIMENSION + SPLINE_DEGREE; i_z++)
                            for (int j_z = 0; j_z < INTERVAL_NUMBER_IN_DIMENSION + SPLINE_DEGREE; j_z++)
                                for (int point_x = 0; point_x < QUADRATURE_POINTS_NUMBER; point_x++) {
                                    int test_index = getSplineIndex(i_x, i_y, i_z);
                                    int trial_index = getSplineIndex(j_x, j_y, j_z);
                                    MAT[test_index][trial_index] += D[i_y][j_y][i_z][j_z][point_x] *
                                                                    B_part(element_x, i_x, j_x,
                                                                           point_x) *
                                                                    QUADRATURE_WEIGHTS[point_x] *
                                                                    J[element_x];
                                }

    for (int i = 0; i < spline_num; i++) {
        for (int j = 0; j < spline_num; j++) {
            for (int k = 0; k < point_num; k++) {
                delete[] C[i][j][k];
            }
            delete[] C[i][j];
        }
        delete[] C[i];
    }
    delete[] C;

    for (int i = 0; i < spline_num; i++) {
        for (int j = 0; j < spline_num; j++) {
            for (int k = 0; k < spline_num; k++) {
                for (int l = 0; l < point_num; l++) {
                    delete[] D[i][j][k][l];
                }
                delete[] D[i][j][k];
            }
            delete[] D[i][j];
        }
        delete[] D[i];
    }

    delete[] D;
}

void rowByRowIntegration() {
    for (int i_x = 0; i_x < SPLINE_NUMBER_IN_DIMENSION; i_x++)
        for (int j_x = std::max(i_x - SPLINE_DEGREE, 0);
             j_x <= std::min(i_x + SPLINE_DEGREE, SPLINE_NUMBER_IN_DIMENSION - 1); j_x++)
            for (int i_y = 0; i_y < SPLINE_NUMBER_IN_DIMENSION; i_y++)
                for (int j_y = std::max(i_y - SPLINE_DEGREE, 0);
                     j_y <= std::min(i_y + SPLINE_DEGREE, SPLINE_NUMBER_IN_DIMENSION - 1); j_y++)
                    for (int i_z = 0; i_z < SPLINE_NUMBER_IN_DIMENSION; i_z++)
                        for (int j_z = std::max(i_z - SPLINE_DEGREE, 0);
                             j_z <= std::min(i_z + SPLINE_DEGREE, SPLINE_NUMBER_IN_DIMENSION - 1); j_z++) {
                            int test_index = getSplineIndex(i_x, i_y, i_z);
                            int trial_index = getSplineIndex(j_x, j_y, j_z);

                            Spline test = {i_x, i_y, i_z};
                            Spline trial = {j_x, j_y, j_z};

                            Interval interval = getCommonInterval(i_x, j_x, i_y, j_y, i_z, j_z);
                            for (int point_x = 0; point_x < QUADRATURE_POINTS_NUMBER; point_x++)
                                for (int point_y = 0; point_y < QUADRATURE_POINTS_NUMBER; point_y++)
                                    for (int point_z = 0; point_z < QUADRATURE_POINTS_NUMBER; point_z++) {
                                        Vector3D point = {point_x, point_y,
                                                          point_z};
                                        MAT[test_index][trial_index] +=
                                                B(interval, test, trial, point) * QUADRATURE_WEIGHTS[point_x] *
                                                QUADRATURE_WEIGHTS[point_y] * QUADRATURE_WEIGHTS[point_z];
                                    }
                        }
}

int main() {

    std::ofstream Matrix("classic.txt");

    initialize_matrix();
    initialize_knot();
    compute_bspline_values();

    classicIntegration();
    sumFactorizationIntegration();
//    rowByRowIntegration();

    for (int i = 0; i < SPLINE_NUMBER; i++) {
        for (int j = 0; j < SPLINE_NUMBER; j++) {
            Matrix << MAT[i][j] << ',';
        }
        Matrix << "\n";
    }

    Matrix.close();
    clear_matrix();
    clear_knot();
    clear_b();
    return 0;
}