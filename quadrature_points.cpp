//
// Created by studz on 30.12.2023.
//

#include "quadrature_points.h"

const double* const Xs[] = {
        nullptr,           nullptr,           gauss_data<2>::X,  gauss_data<3>::X,  gauss_data<4>::X,
        gauss_data<5>::X,  gauss_data<6>::X,  gauss_data<7>::X,  gauss_data<8>::X,  gauss_data<9>::X,
        gauss_data<10>::X
};

const double* const Ws[] = {
        nullptr,           nullptr,           gauss_data<2>::W,  gauss_data<3>::W,  gauss_data<4>::W,
        gauss_data<5>::W,  gauss_data<6>::W,  gauss_data<7>::W,  gauss_data<8>::W,  gauss_data<9>::W,
        gauss_data<10>::W
};