first_matrix = []
second_matrix = []

FIRST_MATRIX_NAME = "classic.txt"
SECOND_MATRIX_NAME = "sum.txt"
EPSILON = 0.00000001
with open(FIRST_MATRIX_NAME) as f:
    for line in f:
        new_row = []
        for value in line.split(","):
            if value != "" and value != "\n":
                new_row.append(float(value))
        first_matrix.append(new_row)

with open(SECOND_MATRIX_NAME) as f:
    for line in f:
        new_row = []
        for value in line.split(","):
            if value != "" and value != "\n":
                new_row.append(float(value))
        second_matrix.append(new_row)

if len(second_matrix) != len(first_matrix):
    raise "Rows count not equal"

valid = 0
invalid = 0
print(len(second_matrix))
invalidIndexes = set()
for i in range(len(second_matrix) - 1, -1, -1):
    if len(second_matrix[i]) != len(first_matrix[i]):
        raise "Columns count not equal"
    for j in range(len(second_matrix[i])):
        if abs(second_matrix[i][j] - first_matrix[i][j]) > EPSILON:
            invalidIndexes.add(i)
            # print(i, j)
            # print(second_matrix[i][j], " ", first_matrix[i][j])
            print(
                SECOND_MATRIX_NAME,
                ": ",
                second_matrix[i][j],
                " ",
                FIRST_MATRIX_NAME,
                ": ",
                first_matrix[i][j],
            )

            invalid += 1
        else:
            valid += 1
print(f"Invalid: {invalid}")
print(f"Valid: {valid}")
print(invalidIndexes)
